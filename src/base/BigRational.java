package base;

import java.math.BigInteger;

public class BigRational implements Comparable<BigRational> {
	protected BigInteger numerator = null;

	protected BigInteger denominator = null;

	/**
	 * 
	 * The default constructor. The numerator will be 0 and the denominator will
	 * be 1.
	 */
	public BigRational() {
		this.numerator = new BigInteger("0");
		this.denominator = new BigInteger("1");
	}

	public BigRational(BigInteger n, BigInteger d) {
		int tmp = d.compareTo(BigInteger.ZERO);
		if (tmp == 0)
			throw new ArithmeticException("The denominator is zero");
		BigInteger gcd = n.gcd(d);
		n = n.divide(gcd);
		d = d.divide(gcd);
		if (tmp > 0) {
			this.numerator = n;
			this.denominator = d;
		} else {
			this.numerator = n.negate();
			this.denominator = d.negate();
		}
	}

	public BigInteger getNumerator() {
		return this.numerator;
	}

	public BigInteger getDenominator() {
		return this.denominator;
	}

	public void reset(BigInteger n, BigInteger d) {
		int tmp = d.compareTo(BigInteger.ZERO);
		if (tmp == 0)
			throw new ArithmeticException("The denominator is zero");
		BigInteger gcd = n.gcd(d);
		n = n.divide(gcd);
		d = d.divide(gcd);
		if (tmp > 0) {
			this.numerator = n;
			this.denominator = d;
		} else {
			this.numerator = n.negate();
			this.denominator = d.negate();
		}
	}

	public BigRational add(BigRational r) {
		BigInteger nr = this.numerator.multiply(r.denominator).add(
				this.denominator.multiply(r.numerator));
		BigInteger dr = this.denominator.multiply(r.denominator);
		return new BigRational(nr, dr);
	}

	public void addWith(BigRational r) {
		BigInteger nr = this.numerator.multiply(r.denominator).add(
				this.denominator.multiply(r.numerator));
		BigInteger dr = this.denominator.multiply(r.denominator);
		this.reset(nr, dr);
	}

	public BigRational multiply(BigRational r) {
		return new BigRational(this.numerator.multiply(r.numerator),
				this.denominator.multiply(r.denominator));
	}

	public void multiplyWith(BigRational r) {
		this.reset(this.numerator.multiply(r.numerator),
				this.denominator.multiply(r.denominator));
	}

	public BigRational divide(BigRational r) {
		if (r.numerator.equals(BigInteger.ZERO))
			throw new ArithmeticException("divide by zero");
		return new BigRational(this.numerator.multiply(r.denominator),
				this.denominator.multiply(r.numerator));
	}

	public void divideBy(BigRational r) {
		if (r.numerator.equals(BigInteger.ZERO))
			throw new ArithmeticException("divide by zero");
		this.reset(this.numerator.multiply(r.denominator),
				this.denominator.multiply(r.numerator));
	}

	public boolean equals(Object o) {
		if (!(o instanceof BigRational))
			return false;
		BigRational r = (BigRational) o;
		return (this.numerator.equals(r.numerator))
				&& (this.denominator.equals(r.denominator));
	}

	public int hashCode() {
		return (this.numerator.toString() + this.denominator.toString())
				.hashCode();
	}

	public String toString() {
		return "(" + this.numerator + "/" + this.denominator + ")";
	}

	public int compareTo(BigRational r) {
		BigInteger tmp1 = this.numerator.multiply(r.denominator), tmp2 = this.denominator
				.multiply(r.numerator);
		return tmp1.compareTo(tmp2);
	}

	public int compareToZero() {
		return this.numerator.compareTo(BigInteger.ZERO);
	}
}
