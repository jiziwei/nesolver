package base;

/**
 * 
 * @author jzw This class provides some useful mathematical functions.
 */
public class Maths {
	/**
	 * 
	 * @param x
	 * @param y
	 * @return This function computes the greatest common divisor of the two
	 *         given long integers. The returned value will always be a positive
	 *         number. If one of the two parameter is 0, the absolute value of
	 *         the other parameter will be return.
	 */
	public static long gcd(long x, long y) {
		x = Math.abs(x);
		if (y == 0) return x;
		y = Math.abs(y);
		if (x == 0) return y;
		while (y != 0) {
			long z = y;
			y = x % y;
			x = z;
		}
		return x;
	}
}
