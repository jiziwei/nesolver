package base;

import exception.*;

/**
 * 
 * @author jzw This class is intended to express rational numbers. We use long
 *         integer to hold the numerator and denominator. However, -2^63 is not
 *         allowed. If any step in the calculation exceeds the scope of long
 *         integers, an OutOfLongException will be thrown. In such conditions,
 *         please try BigRational.
 */
public class Rational implements Comparable<Rational> {
	protected long numerator = 0L;

	protected long denominator = 1L;

	public final static long UPLIMIT = 9223372036854775807L;

	public final static long DOWNLIMIT = -9223372036854775808L;

	/**
	 * 
	 * The default constructor. The numerator will be 0 and the denominator will
	 * be 1.
	 */
	public Rational() {
	}

	public Rational(long n, long d) {
		if (d == 0)
			throw new ArithmeticException("The denominator is zero");
		long gcd = Maths.gcd(n, d);
		n = n / gcd;
		d = d / gcd;
		if ((n == Rational.DOWNLIMIT) || (d == Rational.DOWNLIMIT))
			throw new OutOfLongException();
		if (d > 0) {
			this.numerator = n;
			this.denominator = d;
		} else {
			this.numerator = -n;
			this.denominator = -d;
		}
	}

	public long getNumerator() {
		return this.numerator;
	}

	public long getDenominator() {
		return this.denominator;
	}

	public void reset(long n, long d) {
		if (d == 0)
			throw new ArithmeticException("The denominator is zero");
		long gcd = Maths.gcd(n, d);
		n = n / gcd;
		d = d / gcd;
		if ((n == Rational.DOWNLIMIT) || (d == Rational.DOWNLIMIT))
			throw new OutOfLongException();
		if (d > 0) {
			this.numerator = n;
			this.denominator = d;
		} else {
			this.numerator = -n;
			this.denominator = -d;
		}
	}

	/**
	 * 
	 * @param r
	 * @return
	 * @throws OutOfLongException
	 *             Return a new rational object whose value is the sum of this
	 *             object and the parameter. However, if long integers cannot
	 *             express such a rational number, an exception may be thrown.
	 *             If such a thing happens, please try BigRatinoal. What's more,
	 *             after the calling of this function, both this object and the
	 *             parameter will not change.
	 */
	public Rational add(Rational r) {
		long gcd = Maths.gcd(this.denominator, r.denominator);
		if (this.denominator >= Rational.UPLIMIT / gcd / r.denominator)
			throw new OutOfLongException();
		long m1 = r.denominator / gcd, m2 = this.denominator / gcd;
		if ((Math.abs(this.numerator) >= Rational.UPLIMIT / m1)
				|| (Math.abs(r.numerator) >= Rational.UPLIMIT / m2))
			throw new OutOfLongException();
		long tmp1 = this.numerator * m1, tmp2 = r.numerator * m2;
		if (((tmp2 >= 0) && (tmp1 > Rational.UPLIMIT - tmp2))
				|| ((tmp2 < 0) && (tmp1 <= Rational.DOWNLIMIT - tmp2)))
			throw new OutOfLongException();
		return new Rational(tmp1 + tmp2, this.denominator * m1);
	}

	/**
	 * 
	 * @param r
	 * @throws OutOfLongException
	 *             Let the value of this object be the sum of this object and
	 *             the parameter. However, if long integers cannot express such
	 *             a rational number, an exception may be thrown. If such a
	 *             thing happens, please try BigRatinoal. What's more, after the
	 *             calling of this function, both this object and the parameter
	 *             will not change.
	 */
	public void addWith(Rational r) {
		long gcd = Maths.gcd(this.denominator, r.denominator);
		if (this.denominator >= Rational.UPLIMIT / gcd / r.denominator)
			throw new OutOfLongException();
		long m1 = r.denominator / gcd, m2 = this.denominator / gcd;
		if ((Math.abs(this.numerator) >= Rational.UPLIMIT / m1)
				|| (Math.abs(r.numerator) >= Rational.UPLIMIT / m2))
			throw new OutOfLongException();
		long tmp1 = this.numerator * m1, tmp2 = r.numerator * m2;
		if (((tmp2 >= 0) && (tmp1 > Rational.UPLIMIT - tmp2))
				|| ((tmp2 < 0) && (tmp1 <= Rational.DOWNLIMIT - tmp2)))
			throw new OutOfLongException();
		this.reset(tmp1 + tmp2, this.denominator * m1);
	}

	public Rational multiply(Rational r) {
		long n1 = this.numerator, d1 = this.denominator, n2 = r.numerator, d2 = r.denominator;
		long gcd1 = Maths.gcd(n1, d2), gcd2 = Maths.gcd(n2, d1);
		n1 = n1 / gcd1;
		d2 = d2 / gcd1;
		n2 = n2 / gcd2;
		d1 = d1 / gcd2;
		if ((Math.abs(n1) >= Rational.UPLIMIT / Math.abs(n2))
				|| (d1 >= Rational.UPLIMIT / d2))
			throw new OutOfLongException();
		return new Rational(n1 * n2, d1 * d2);
	}

	public void multiplyWith(Rational r) {
		long n1 = this.numerator, d1 = this.denominator, n2 = r.numerator, d2 = r.denominator;
		long gcd1 = Maths.gcd(n1, d2), gcd2 = Maths.gcd(n2, d1);
		n1 = n1 / gcd1;
		d2 = d2 / gcd1;
		n2 = n2 / gcd2;
		d1 = d1 / gcd2;
		if ((Math.abs(n1) >= Rational.UPLIMIT / Math.abs(n2))
				|| (d1 >= Rational.UPLIMIT / d2))
			throw new OutOfLongException();
		this.reset(n1 * n2, d1 * d2);
	}

	public Rational divide(Rational r) {
		if (r.numerator == 0)
			throw new ArithmeticException("divide by zero");
		long n1 = this.numerator, d1 = this.denominator, n2 = r.denominator, d2 = r.numerator;
		long gcd1 = Maths.gcd(n1, d2), gcd2 = Maths.gcd(n2, d1);
		n1 = n1 / gcd1;
		d2 = d2 / gcd1;
		n2 = n2 / gcd2;
		d1 = d1 / gcd2;
		if ((Math.abs(n1) >= Rational.UPLIMIT / n2)
				|| (d1 >= Rational.UPLIMIT / Math.abs(d2)))
			throw new OutOfLongException();
		return new Rational(n1 * n2, d1 * d2);
	}

	public void divideBy(Rational r) {
		if (r.numerator == 0)
			throw new ArithmeticException("divide by zero");
		long n1 = this.numerator, d1 = this.denominator, n2 = r.denominator, d2 = r.numerator;
		long gcd1 = Maths.gcd(n1, d2), gcd2 = Maths.gcd(n2, d1);
		n1 = n1 / gcd1;
		d2 = d2 / gcd1;
		n2 = n2 / gcd2;
		d1 = d1 / gcd2;
		if ((Math.abs(n1) >= Rational.UPLIMIT / n2)
				|| (d1 >= Rational.UPLIMIT / Math.abs(d2)))
			throw new OutOfLongException();
		this.reset(n1 * n2, d1 * d2);
	}

	public boolean equals(Object o) {
		if (!(o instanceof Rational))
			return false;
		Rational r = (Rational) o;
		return (this.numerator == r.numerator)
				&& (this.denominator == r.denominator);
	}

	public int hashCode() {
		return ("" + this.numerator + this.denominator).hashCode();
	}

	public String toString() {
		return "(" + this.numerator + "/" + this.denominator + ")";
	}

	public int compareTo(Rational r) {
		if ((Math.abs(this.numerator) >= Rational.UPLIMIT / r.denominator)
				|| (Math.abs(r.numerator) >= Rational.UPLIMIT
						/ this.denominator))
			throw new OutOfLongException();
		long tmp1 = this.numerator * r.denominator, tmp2 = this.denominator
				* r.numerator;
		if (tmp1 == tmp2)
			return 0;
		else if (tmp1 < tmp2)
			return -1;
		else
			return 1;
	}

	public int compareToZero() {
		if (this.numerator < 0)
			return -1;
		else if (this.numerator == 0)
			return 0;
		else
			return 1;
	}
}
