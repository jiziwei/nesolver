package exception;

/**
 * 
 * @author jzw This class denotes the exception about dimensions. For example,
 *         the given number of pure strategies doesn't match the dimension of
 *         mixed strategies, or some vector has zero length.
 */
public class DimensionException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
