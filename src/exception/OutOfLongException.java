package exception;

/**
 * 
 * @author jzw This class denotes that the result of some computation get out of
 *         the bound of long integer.
 */
public class OutOfLongException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
