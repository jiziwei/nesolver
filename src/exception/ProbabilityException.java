package exception;

/**
 * 
 * @author jzw This class is intended to denote the exception about probability.
 *         For example, some numbers are less than zero, or the sum of all
 *         numbers before re-scaling is less than zero.
 */
public class ProbabilityException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}