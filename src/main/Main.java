package main;

import base.*;
//import exception.*;
import nesolver.*;
import strategy.*;
import java.io.*;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner(new File("game.in"));
		int m = in.nextInt(), n = in.nextInt();
		long[][] a = new long[m][n], b = new long[m][n];
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				a[i][j] = in.nextLong();
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				b[i][j] = in.nextLong();
		MixedStrategy<Rational>[] tmp = NESolver.generalNEForGeneralGame(m, n, a, b);
		System.out.println(tmp[0]);
		System.out.println(tmp[1]);
	}
}
