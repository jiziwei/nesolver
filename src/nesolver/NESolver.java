package nesolver;

import base.*;
import exception.*;
import strategy.*;
import java.util.*;

public class NESolver {
	public static MixedStrategy<Rational>[] generalNEForGeneralGame(int m, int n,
			long[][] a, long[][] b) throws DimensionException,
			OutOfLongException {
		if ((m <= 0) || (n <= 0) || (a.length != m) || (a[0].length != n)
				|| (b.length != m) || (b[0].length != n))
			throw new DimensionException();
		long tmp = 1;
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++) {
				if (a[i][j] < tmp)
					tmp = a[i][j];
				if (b[i][j] < tmp)
					tmp = b[i][j];
			}
		if (tmp == 1)
			tmp = 0;
		else
			tmp = -tmp + 1;
		long[][] m1 = new long[n][m + n + 1];
		long[][] m2 = new long[m][m + n + 1];
		for (int i = 0; i < n; i++) {
			m1[i][m + n] = 1L;
			for (int j = 0; j < m; j++)
				m1[i][j] = b[j][i] + tmp;
			for (int j = m; j < m + n; j++)
				m1[i][j] = 0L;
			m1[i][i + m] = 1L;
		}
		for (int i = 0; i < m; i++) {
			m2[i][m + n] = 1L;
			for (int j = m; j < m + n; j++)
				m2[i][j] = a[i][j - m] + tmp;
			for (int j = 0; j < m; j++)
				m2[i][j] = 0L;
			m2[i][i] = 1L;
		}
		HashMap<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> map2 = new HashMap<Integer, Integer>();
		for (int i = 0; i < n; i++)
			map1.put(i, i + m);
		for (int i = 0; i < m; i++)
			map2.put(i, i);
		int c = NESolver.integerPivoting(0, n, m + n + 1, map1, 1, m1);
		
		/*for (int i = 0; i < n; i++) {
			for (int j = 0; j < m + n + 1; j++)
				System.out.print(m1[i][j] + " ");
			System.out.println();
		}
		System.out.println(map1);
		System.out.println(c + "\n");*/
		
		while (true) {
			c = NESolver.integerPivoting(c, m, m + n + 1, map2, m2[0][map2.get(0)],
					m2);
			
			/*for (int i = 0; i < m; i++) {
				for (int j = 0; j < m + n + 1; j++)
					System.out.print(m2[i][j] + " ");
				System.out.println();
			}
			System.out.println(map2);
			System.out.println(c + "\n");*/
			
			if (c == 0)
				break;
			
			c = NESolver.integerPivoting(c, n, m + n + 1, map1, m1[0][map1.get(0)],
					m1);
			
			/*for (int i = 0; i < n; i++) {
				for (int j = 0; j < m + n + 1; j++)
					System.out.print(m1[i][j] + " ");
				System.out.println();
			}
			System.out.println(map1);
			System.out.println(c + "\n");*/
			
			if (c == 0)
				break;
		}
		Rational[] p1 = new Rational[m];
		for (int i = 0; i < m; i++)
			p1[i] = new Rational();
		for (int i = 0; i < n; i++) {
			int j = map1.get(i);
			if (j < m)
				p1[j] = new Rational(m1[i][m + n], m1[i][j]);
		}
		Rational[] p2 = new Rational[n];
		for (int i = 0; i < n; i++)
			p2[i] = new Rational();
		for (int i = 0; i < m; i++) {
			int j = map2.get(i);
			if (j >= m)
				p2[j - m] = new Rational(m2[i][m + n], m2[i][j]);
		}
		@SuppressWarnings("unchecked")
		MixedStrategy<Rational>[] ret = new MixedStrategy[2];
		try {
			ret[0] = new MixedStrategy<Rational>(m, p1);
			ret[1] = new MixedStrategy<Rational>(n, p2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	/**
	 * 
	 * @param c
	 *            Denote the non-basic variable which is going to be basic.
	 * @param m
	 *            Denote the height of the matrix, starting with 1.
	 * @param n
	 *            Denote the width of the matrix, starting with 1.
	 * @param map
	 *            Denote the map from the rows to the basic columns.
	 * @param d
	 *            Denote the current pivot element.
	 * @param x
	 *            Denote the matrix.
	 * @return Return the new non-basic column.
	 */
	protected static int integerPivoting(int c, int m, int n,
			Map<Integer, Integer> map, long d, long[][] x) {
		int num = 0, pos = 0;
		Rational rmax = new Rational();
		for (int i = 0; i < m; i++) {
			if (x[i][n - 1] < 0)
				throw new Error("design error");
			if (x[i][n - 1] == 0)
				throw new Error("degenerate game");
			if (x[i][c] <= 0)
				continue;
			Rational rtmp = new Rational(x[i][c], x[i][n - 1]);
			int tmp = rmax.compareTo(rtmp);
			if (tmp < 0) {
				rmax = rtmp;
				num = 1;
				pos = i;
			} else if (tmp == 0)
				num++;
		}
		if (rmax.getNumerator() == 0)
			throw new Error("design error");
		if (num > 1)
			throw new Error("degenerate game");
		long factor = x[pos][c], com1 = Rational.UPLIMIT / Math.abs(factor);
		for (int i = 0; i < m; i++) {
			if (i == pos)
				continue;
			long tmp = x[i][c], com2 = Rational.UPLIMIT / Math.abs(tmp);
			for (int j = 0; j < n; j++) {
				if ((Math.abs(x[i][j]) >= com1)
						|| (Math.abs(x[pos][j]) >= com2))
					throw new OutOfLongException();
				long tmp1 = x[i][j] * factor, tmp2 = -(x[pos][j] * tmp);
				if (((tmp2 >= 0) && (tmp1 > Rational.UPLIMIT - tmp2))
						|| ((tmp2 < 0) && (tmp1 <= Rational.DOWNLIMIT - tmp2)))
					throw new OutOfLongException();
				x[i][j] = (tmp1 + tmp2) / d;
			}
		}
		int ret = map.get(pos);
		map.put(pos, c);
		return ret;
	}
}
