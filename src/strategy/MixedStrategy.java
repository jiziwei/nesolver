package strategy;

import base.*;
import exception.*;

public class MixedStrategy<T> {
	protected int numberOfPureStrategies;

	protected T[] probabilities;

	@SuppressWarnings("unchecked")
	public MixedStrategy(int n, T[] p) {
		if ((n != p.length) || (n == 0))
			throw new DimensionException();
		if (!this.sumChecker(p))
			throw new ProbabilityException();
		this.numberOfPureStrategies = n;
		this.probabilities = (T[]) new Object[p.length];
		for (int i = 0; i < p.length; i++)
			this.probabilities[i] = p[i];
		this.rescale();
	}

	public void reset(int n, T[] p) {
		if ((n != p.length) || (n == 0))
			throw new DimensionException();
		if (!this.sumChecker(p))
			throw new ProbabilityException();
		this.numberOfPureStrategies = n;
		for (int i = 0; i < p.length; i++)
			this.probabilities[i] = p[i];
		this.rescale();
	}

	protected boolean sumChecker(T[] p) {
		if (p[0] instanceof Rational) {
			Rational rsum = new Rational();
			for (T t : p) {
				Rational tmp = (Rational) t;
				if (tmp.compareToZero() < 0) return false;
				rsum.addWith(tmp);
			}
			if (rsum.compareToZero() <= 0) return false;
			return true;
		}
		return true;
	}
	
	protected void rescale() {
		if (this.probabilities[0] instanceof Rational) {
			Rational rsum = new Rational();
			for (T t : this.probabilities) {
				Rational tmp = (Rational) t;
				rsum.addWith(tmp);
			}
			for (int i = 0; i < this.numberOfPureStrategies; i++) {
				((Rational) this.probabilities[i]).divideBy(rsum);
			}
		}
	}
	
	public String toString() {
		String ret = "[";
		for (int i = 0; i < this.numberOfPureStrategies - 1; i++)
			ret = ret + this.probabilities[i].toString() + ", ";
		ret = ret + this.probabilities[this.numberOfPureStrategies - 1].toString() + "]";
		return ret;
	}
}
